const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

var mainWindow = null;

app.on('window-all-closed', function(){
    app.quit();
});

app.on('ready', function(){
    var subpy = require('child_process').spawn('python3', ['./hello.py']);
    var rq = require('request-promise');
    var mainAddr = 'http://localhost:5000';

    var openWindow = function(){
        mainWindow = new BrowserWindow({
            width : 1280,
            height : 1024
        });

        mainWindow.loadURL('file:///home/ml/electronJS/ui/shopping-cart/index.html');
        mainWindow.webContents.openDevTools()
        mainWindow.on('closed', function(){
            mainWindow = null;
            subpy.kill('SIGINT')
        })
    }

    var startUp = function(){
        rq(mainAddr)
          .then(function(htmlString){
            console.log('server started!');
            openWindow();
          })
          .catch(function(err){
            console.log('waiting for the server start...');
            startUp();
          });
      };

    startUp();
})